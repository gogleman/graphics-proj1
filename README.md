# 3D camera
3D camera is a Java Swing application presenting camera movement around the scene

## Functionalities implemented
* Presenting objects on the scene 
* Moving camera: left/right/up/down/backward/forward
* Rotate camera around axis x/y/z
* Zoom in/out camera


## Technologies used
* Java
* Swing library
* Maven
* MVP pattern
