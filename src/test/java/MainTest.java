import model.DefaultCuboids;
import model.Scene;
import presenter.ScenePresenter;
import view.AppFrame;


public class MainTest {

    public static void main(String [] args){

        DefaultCuboids defaultCuboids = new DefaultCuboids();
        //Cuboid cuboid1 = new Cuboid(defaultCuboids.getCuboid1Points3d());

        //System.out.println("Cuboid1:" + cuboid1);
        Scene scene = new Scene();
        scene.addObject(defaultCuboids.getCuboids().get(0));
        scene.addObject(defaultCuboids.getCuboids().get(1));
        scene.addObject(defaultCuboids.getCuboids().get(2));
        scene.addObject(defaultCuboids.getCuboids().get(3));

        //System.out.println(scene);
        System.out.print("Point no 0: ");
        System.out.println(scene.getObjects().get(0).getPoints3d().get(0));
        ScenePresenter scenePresenter = new ScenePresenter();
        AppFrame af = new AppFrame(scenePresenter,scene);
        scenePresenter.setScene(scene);
        scenePresenter.setAppFrame(af);
        scenePresenter.setSceneOnFrame();

        af.open();

        //af.drawScene(scene);


    }

}
