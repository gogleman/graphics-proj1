import algorithms.Algorithms;
import core.Point2D;
import core.Point3D;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class AlgorithmsTest {

    private Point3D point3D;
    //private int DISTANCE = 600;

    @Before
    public void prepare(){
        point3D = new Point3D(100,100,100);

    }

    @Test
    public void transform2dTo3d(){
        Point2D point2D = Algorithms.transform3dTo2d(point3D, 1560);
        Assert.assertEquals(point2D,new Point2D(93,93));
    }

    @Test
    public void transformPoint3dToMatrix(){

    }
}
