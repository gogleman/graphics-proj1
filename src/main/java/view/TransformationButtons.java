package view;

import javax.swing.*;
import java.util.EnumMap;

public class TransformationButtons {

    public enum ButtonType {
        MOVE_LEFT,
        MOVE_RIGHT,
        MOVE_UP,
        MOVE_DOWN,
        MOVE_FORWARD,
        MOVE_BACKWARD,
        ROTATE_X,
        ROTATE_X_MINUS,
        ROTATE_Y,
        ROTATE_Y_MINUS,
        ROTATE_Z,
        ROTATE_Z_MINUS,
        ZOOM_IN,
        ZOOM_OUT
    }

    private JButton move_left;
    private JButton move_right;
    private JButton move_up;
    private JButton move_down;
    private JButton move_forward;
    private JButton move_backward;
    private JButton rotate_x;
    private JButton rotate_x_minus;
    private JButton rotate_y;
    private JButton rotate_y_minus;
    private JButton rotate_z;
    private JButton rotate_z_minus;
    private JButton zoom_in;
    private JButton zoom_out;

    public static EnumMap<ButtonType,JButton> transformationButtonsMap;

    public TransformationButtons() {

        createButtons();
        addToMap();
    }

    private void createButtons() {
        move_left = new JButton("Move left");
        move_right = new JButton("Move right");
        move_up = new JButton("Move up");
        move_down = new JButton("Move down");
        move_forward = new JButton("Move forward");
        move_backward = new JButton("Move backward");
        rotate_x = new JButton("Rotate X");
        rotate_x_minus = new JButton("Rotate -X");
        rotate_y = new JButton("Rotate Y");
        rotate_y_minus = new JButton("Rotate -Y");
        rotate_z = new JButton("Rotate Z");
        rotate_z_minus = new JButton("Rotate -Z");
        zoom_in = new JButton("Zoom in");
        zoom_out = new JButton("Zoom out");
    }

    private void addToMap(){

        transformationButtonsMap = new EnumMap<ButtonType, JButton>(ButtonType.class);

        transformationButtonsMap.put(ButtonType.MOVE_LEFT,move_left);
        transformationButtonsMap.put(ButtonType.MOVE_RIGHT,move_right);
        transformationButtonsMap.put(ButtonType.MOVE_UP,move_up);
        transformationButtonsMap.put(ButtonType.MOVE_DOWN,move_down);
        transformationButtonsMap.put(ButtonType.MOVE_FORWARD,move_forward);
        transformationButtonsMap.put(ButtonType.MOVE_BACKWARD,move_backward);
        transformationButtonsMap.put(ButtonType.ROTATE_X, rotate_x);
        transformationButtonsMap.put(ButtonType.ROTATE_X_MINUS, rotate_x_minus);
        transformationButtonsMap.put(ButtonType.ROTATE_Y, rotate_y);
        transformationButtonsMap.put(ButtonType.ROTATE_Y_MINUS, rotate_y_minus);
        transformationButtonsMap.put(ButtonType.ROTATE_Z, rotate_z);
        transformationButtonsMap.put(ButtonType.ROTATE_Z_MINUS, rotate_z_minus);
        transformationButtonsMap.put(ButtonType.ZOOM_IN,zoom_in);
        transformationButtonsMap.put(ButtonType.ZOOM_OUT,zoom_out);
    }

    public EnumMap<ButtonType, JButton> getButtonsMap() {
        return transformationButtonsMap;
    }
}
