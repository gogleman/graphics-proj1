package view;

import core.Line2D;
import model.Scene;
import model.SceneObject;

import javax.swing.*;
import java.awt.*;
import java.util.LinkedList;

import static view.AppFrame.FRAME_HEIGHT;
import static view.AppFrame.FRAME_WIDTH;

public class ScenePanel extends JPanel{

    private Color COLOR = Color.WHITE;
    private Scene scene;


    public ScenePanel() {
    }

    @Override
    public void paintComponent(Graphics g){
        super.paintComponent(g);
        g.setColor(COLOR);
        drawObject(g);

    }

    private void drawObject(Graphics g){

        for (SceneObject object : scene.getObjects()) {

            LinkedList<Line2D> lines2d = object.getLines2d();
            for (Line2D line2D : lines2d) {
                int x1 = line2D.getStartPoint().getX()+FRAME_WIDTH/2;
                int y1 = -(line2D.getStartPoint().getY()-FRAME_HEIGHT/2);
                int x2 = line2D.getEndPoint().getX()+FRAME_WIDTH/2;
                int y2 = -(line2D.getEndPoint().getY()-FRAME_HEIGHT/2);
                g.drawLine(x1, y1, x2, y2);
            }
        }
    }

    public void setScene(Scene scene) {
        this.scene = scene;
    }
}
