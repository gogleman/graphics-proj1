package view;

import model.Scene;
import presenter.ScenePresenter;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.EnumMap;
import java.util.Map;

public class AppFrame extends JFrame implements ActionListener, KeyListener{

    private ScenePresenter scenePresenter;
    private Container container;
    private ScenePanel scenePanel;
    private JPanel buttonsPanel;
    private JButton close;
    private EnumMap<TransformationButtons.ButtonType, JButton> transformationButtonsMap;
    private Scene scene;
    public final static int FRAME_WIDTH = 1600;
    public final static int FRAME_HEIGHT = 1000;

    public AppFrame(){

        super("Project 1");
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setSize(FRAME_WIDTH,FRAME_HEIGHT);
        prepareGUI();

    }

    public AppFrame(ScenePresenter scenePresenter, Scene scene){
        this();
        this.setScenePresenter(scenePresenter);
        this.scene = scene;
        setSceneOnPanel(scene);

     }

    private void prepareGUI(){
        Container container;
        scenePanel = new ScenePanel();
        buttonsPanel = new JPanel();

        scenePanel.setBackground(Color.BLUE);
        buttonsPanel.setBackground(Color.RED);

        container = this.getContentPane();
        container.add(scenePanel);
        container.add(buttonsPanel,BorderLayout.AFTER_LAST_LINE);
        this.setFocusable(true);
        addKeyListener(this);

        prepareButtons();
    }

    private void prepareButtons(){
        TransformationButtons transformationButtons = new TransformationButtons();
        transformationButtonsMap = transformationButtons.getButtonsMap();

        for(Map.Entry<TransformationButtons.ButtonType,JButton> entry : transformationButtonsMap.entrySet()){
            buttonsPanel.add(entry.getValue());
            entry.getValue().addActionListener(this);
        }

        close = new JButton("Close");
        close.addActionListener(this);
        buttonsPanel.add(close);
    }


    public void open(){
        this.setVisible(true);
    }
    public void close(){ this.dispose(); }

    private void setSceneOnPanel(Scene scene){
        this.scenePanel.setScene(scene);
    }

    public void setScenePresenter(ScenePresenter scenePresenter) {
        this.scenePresenter = scenePresenter;
    }

    public void setScene(Scene scene) {
        this.scene = scene;
    }

    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();

        if(source == close){
            scenePresenter.closeButtonClicked();
        } else {
            for(Map.Entry entry : transformationButtonsMap.entrySet()){
                if(entry.getValue() == source ){
                    scenePresenter.transformationButtonClicked((TransformationButtons.ButtonType)entry.getKey());
                }
            }
        }
    }

    public void keyTyped(KeyEvent e) {
    }

    public void keyPressed(KeyEvent e) {

        switch (e.getKeyCode()){
            case KeyEvent.VK_RIGHT:
                transformationButtonsMap.get(TransformationButtons.ButtonType.MOVE_RIGHT).doClick();
                break;
            case KeyEvent.VK_LEFT:
                transformationButtonsMap.get(TransformationButtons.ButtonType.MOVE_LEFT).doClick();
                break;
            case KeyEvent.VK_UP:
                transformationButtonsMap.get(TransformationButtons.ButtonType.MOVE_UP).doClick();
                break;
            case KeyEvent.VK_DOWN:
                transformationButtonsMap.get(TransformationButtons.ButtonType.MOVE_DOWN).doClick();
                break;
            case KeyEvent.VK_W:
                transformationButtonsMap.get(TransformationButtons.ButtonType.MOVE_FORWARD).doClick();
                break;
            case KeyEvent.VK_S:
                transformationButtonsMap.get(TransformationButtons.ButtonType.MOVE_BACKWARD).doClick();
                break;
            case KeyEvent.VK_A:
                transformationButtonsMap.get(TransformationButtons.ButtonType.ROTATE_Y_MINUS).doClick();
                break;
            case KeyEvent.VK_D:
                transformationButtonsMap.get(TransformationButtons.ButtonType.ROTATE_Y).doClick();
                break;
            case KeyEvent.VK_Z:
                transformationButtonsMap.get(TransformationButtons.ButtonType.ZOOM_IN).doClick();
                break;
            case KeyEvent.VK_X:
                transformationButtonsMap.get(TransformationButtons.ButtonType.ZOOM_OUT).doClick();
                break;
            case KeyEvent.VK_R:
                transformationButtonsMap.get(TransformationButtons.ButtonType.ROTATE_X).doClick();
                break;
            case KeyEvent.VK_F:
                transformationButtonsMap.get(TransformationButtons.ButtonType.ROTATE_X_MINUS).doClick();
                break;
            case KeyEvent.VK_G:
                transformationButtonsMap.get(TransformationButtons.ButtonType.ROTATE_Z).doClick();
                break;
            case KeyEvent.VK_H:
                transformationButtonsMap.get(TransformationButtons.ButtonType.ROTATE_Z_MINUS).doClick();
                break;
        }
    }

    public void keyReleased(KeyEvent e) {
    }
}
