package model;

import core.Line2D;
import core.Line3D;
import core.Point2D;
import core.Point3D;

import java.util.HashMap;
import java.util.LinkedList;

public interface SceneObject {

    HashMap<Integer, Point3D> getPoints3d();
    LinkedList<Line3D> getLines3d();
    HashMap<Integer, Point2D> getPoints2d();
    LinkedList<Line2D> getLines2d();
    void recalculatePoints();
    void setDistance(int distance);
    int getDistance();
}
