package model;

import algorithms.Algorithms;
import core.Line2D;
import core.Line3D;
import core.Point2D;
import core.Point3D;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class Cuboid implements SceneObject{

    private HashMap<Integer, Point3D> points3d;
    private LinkedList<Line3D> lines3d;
    private HashMap<Integer, Point2D> points2d;
    private LinkedList<Line2D> lines2d;
    private static final int INITIAL_DISTANCE = 1560;
    private int distance = INITIAL_DISTANCE;

    private Cuboid(){

        points3d = new HashMap<Integer, Point3D>();
        lines3d = new LinkedList<Line3D>();
        points2d = new HashMap<Integer, Point2D>();
        lines2d = new LinkedList<Line2D>();
    }

    public Cuboid(HashMap<Integer, Point3D> points3d){
        this();
        this.points3d = points3d;
        create3dLines();
        create2dPoints();
        create2dLines();

    }

    private void create3dLines(){
        lines3d.add(new Line3D(points3d.get(0),points3d.get(1)));
        lines3d.add(new Line3D(points3d.get(1),points3d.get(2)));
        lines3d.add(new Line3D(points3d.get(2),points3d.get(3)));
        lines3d.add(new Line3D(points3d.get(3),points3d.get(0)));
        lines3d.add(new Line3D(points3d.get(0),points3d.get(4)));
        lines3d.add(new Line3D(points3d.get(1),points3d.get(5)));
        lines3d.add(new Line3D(points3d.get(2),points3d.get(6)));
        lines3d.add(new Line3D(points3d.get(3),points3d.get(7)));
        lines3d.add(new Line3D(points3d.get(4),points3d.get(5)));
        lines3d.add(new Line3D(points3d.get(5),points3d.get(6)));
        lines3d.add(new Line3D(points3d.get(6),points3d.get(7)));
        lines3d.add(new Line3D(points3d.get(7),points3d.get(4)));
    }

    private void create2dPoints(){
        for(Map.Entry<Integer,Point3D> entry : points3d.entrySet()){
            int id = entry.getKey();
            Point3D point3D = entry.getValue();
            points2d.put(id, Algorithms.transform3dTo2d(point3D,distance));
        }
    }

    private void create2dLines(){

        lines2d.add(new Line2D(points2d.get(0),points2d.get(1)));
        lines2d.add(new Line2D(points2d.get(1),points2d.get(2)));
        lines2d.add(new Line2D(points2d.get(2),points2d.get(3)));
        lines2d.add(new Line2D(points2d.get(3),points2d.get(0)));
        lines2d.add(new Line2D(points2d.get(0),points2d.get(4)));
        lines2d.add(new Line2D(points2d.get(1),points2d.get(5)));
        lines2d.add(new Line2D(points2d.get(2),points2d.get(6)));
        lines2d.add(new Line2D(points2d.get(3),points2d.get(7)));
        lines2d.add(new Line2D(points2d.get(4),points2d.get(5)));
        lines2d.add(new Line2D(points2d.get(5),points2d.get(6)));
        lines2d.add(new Line2D(points2d.get(6),points2d.get(7)));
        lines2d.add(new Line2D(points2d.get(7),points2d.get(4)));
    }

    public void recalculatePoints (){
        this.points2d = new HashMap<Integer, Point2D>();
        create2dPoints();
        this.lines3d = new LinkedList<Line3D>();
        create3dLines();
        this.lines2d = new LinkedList<Line2D>();
        create2dLines();
    }

    public HashMap<Integer, Point3D> getPoints3d() {
        return points3d;
    }

    public LinkedList<Line3D> getLines3d() {
        return lines3d;
    }

    public HashMap<Integer, Point2D> getPoints2d() {
        return points2d;
    }

    public LinkedList<Line2D> getLines2d() {
        return lines2d;
    }

    public void setDistance(int distance){
        this.distance = distance;
        recalculatePoints();
    }

    public int getDistance() {
        return distance;
    }

    @Override
    public String toString() {
        return "Cuboid{" +
                "points3d=" + points3d +
                ", points2d=" + points2d +
                '}';
    }
}